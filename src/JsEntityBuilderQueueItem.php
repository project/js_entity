<?php

namespace Drupal\js_entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;
use Drupal\js_entity\Queue\QueueItemInterface;

/**
 * Define JS entity builder queue item.
 */
class JsEntityBuilderQueueItem implements QueueItemInterface {

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * @var string
   */
  protected $itemKey;

  /**
   * @var \Drupal\js_entity\Entity\JsEntityRenderProfileInterface
   */
  protected $renderProfile;

  /**
   * JS entity builder queue item constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   A content entity instance.
   * @param \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile
   *   A content entity render profile instance.
   */
  public function __construct(
    ContentEntityInterface $entity,
    JsEntityRenderProfileInterface $render_profile
  ) {
    $this->entity = $entity;
    $this->renderProfile = $render_profile;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemKey() {
    return $this->itemKey;
  }

  /**
   * {@inheritDoc}
   */
  public function getItemCacheTags() {
    $cache_tags = $this->entity->getCacheTags();

    return $this->aggregateEntityReferenceCacheTags(
      $this->entity, $cache_tags
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setItemKey($item_key) {
    $this->itemKey = $item_key;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createItem() {
    /** @var \Drupal\js_entity\JsEntityBuilderInterface $builder */
    $builder = \Drupal::service('js_entity.builder');

    return $builder->build(
      $this->entity,
      $this->renderProfile
    );
  }

  /**
   * Aggregate entity reference cache tags.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The host entity instance.
   * @param array $cache_tags
   *   An array of existing cache tags.
   *
   * @return array
   *   An array of cache tags based on the entity host.
   */
  protected function aggregateEntityReferenceCacheTags(EntityInterface $entity, &$cache_tags = []) {
    $excluded_cache_tags = $this
      ->renderProfile
      ->getCacheTagExcludedEntities();

    foreach ($entity->referencedEntities() as $referenced_entity) {
      if (! $referenced_entity instanceof ContentEntityInterface) {
        continue;
      }
      $cache_tags = Cache::mergeTags(
        $cache_tags,
        $this->filteredEntityCacheTags(
          $referenced_entity, $excluded_cache_tags
        )
      );
      $this->aggregateEntityReferenceCacheTags($referenced_entity, $cache_tags);
    }

    return $cache_tags;
  }

  /**
   * Filter out excluded cache tags.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity instance.
   * @param array $excluded_cache_tags
   *   An array of excluded cache tag entity types.
   *
   * @return array
   *   An array of non-excluded entity cache tags.
   */
  protected function filteredEntityCacheTags(
    EntityInterface $entity,
    $excluded_cache_tags = []
  ) {
    return array_filter($entity->getCacheTags(),
      function ($entity_cache_tag) use ($excluded_cache_tags) {
        list($tag_type,) = explode(':', $entity_cache_tag);

        return !in_array($tag_type, $excluded_cache_tags);
      });
  }
}
