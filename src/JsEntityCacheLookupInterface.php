<?php

namespace Drupal\js_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

interface JsEntityCacheLookupInterface {

  /**
   * Entity lookup cache data.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity type.
   * @param \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile
   *   The content entity render profile.
   *
   * @return array
   *   The JS entity cache data array.
   */
  public function entityLookup(ContentEntityInterface $entity, JsEntityRenderProfileInterface $render_profile);
}
