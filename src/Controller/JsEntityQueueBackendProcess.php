<?php

namespace Drupal\js_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\js_entity\JsEntityCacheLookupInterface;
use Drupal\js_entity\JsEntityProcessQueueInterface;
use Drupal\js_entity\Queue\QueueItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Define the js entity queue backend process controller.
 */
class JsEntityQueueBackendProcess extends ControllerBase {

  /**
   * @var \Drupal\js_entity\JsEntityCacheLookupInterface
   */
  protected $cacheLookup;

  /**
   * @var \Drupal\js_entity\JsEntityProcessQueueInterface
   */
  protected $queueProcess;

  /**
   * JS entity queue backend process constructor.
   *
   * @param \Drupal\js_entity\JsEntityCacheLookupInterface $cache_lookup
   * @param \Drupal\js_entity\JsEntityProcessQueueInterface $queue_process
   */
  public function __construct(
    JsEntityCacheLookupInterface $cache_lookup,
    JsEntityProcessQueueInterface $queue_process
  ) {
    $this->cacheLookup = $cache_lookup;
    $this->queueProcess = $queue_process;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('js_entity.cache_lookup'),
      $container->get('js_entity.queue.process')
    );
  }

  /**
   * Execute the queue backend process.
   *
   * @param null $item_id
   *   The queue item identifier.
   *
   * @return array|\Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response object.
   *
   * @throws \HttpException
   */
  public function execute($item_id = NULL) {
    $item = $this->queueProcess->processItem($item_id);

    if (!isset($item_id) || $item === FALSE) {
      throw new \HttpException(
        'Unable to execute queue process due to missing item.'
      );
    }

    return new JsonResponse(
      $this->buildCacheData($item->data)
    );
  }

  /**
   * Build cache data.
   *
   * @param \Drupal\js_entity\Queue\QueueItemInterface $item
   *   The queue item.
   *
   * @return array
   *   The cache data array.
   */
  protected function buildCacheData(QueueItemInterface $item) {
    $cache_id = $item->getItemKey();
    $cache_object = $this->cacheLookup->getCacheData($cache_id);

    return $cache_object !== FALSE && isset($cache_object->data)
      ? $cache_object->data
      : [];
  }
}
