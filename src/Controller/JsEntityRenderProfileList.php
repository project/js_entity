<?php

namespace Drupal\js_entity\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Define Js entity render profile list.
 */
class JsEntityRenderProfileList extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
        'label' => $this->t('Label'),
      ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    return [
        'label' => $entity->label(),
      ] + parent::buildRow($entity);
  }
}
