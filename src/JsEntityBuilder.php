<?php

namespace Drupal\js_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

/**
 * Define JS entity builder.
 */
class JsEntityBuilder implements JsEntityBuilderInterface {

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Define JS entity builder constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   A renderer instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   A entity type manager instance.
   */
  public function __construct(
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build(
    ContentEntityInterface $entity,
    JsEntityRenderProfileInterface $render_profile
  ) {
    $view_mode = $render_profile->getConfigViewMode();

    $display = $this->loadEntityViewDisplay(
      $entity->getEntityTypeId(), $entity->bundle(), $view_mode
    );

    if (!$display instanceof EntityDisplayInterface) {
      return [];
    }
    $build = [];
    $components = $render_profile->getConfigComponents();

    foreach ($display->getComponents() as $field_name => $options) {
      $field = $entity->{$field_name};

      if (!$field instanceof FieldItemListInterface) {
        continue;
      }
      $element = $field->view($options);

      $elements = array_intersect_key(
        $element, array_flip(Element::children($element))
      );
      $property_name = isset($components[$field_name]['property_name'])
        ? $components[$field_name]['property_name']
        : $field_name;

      $build[$property_name] = $this->renderElements(
        $options['type'], $elements
      );
    }

    return $build;
  }

  /**
   * Render elements based on widget type.
   *
   * @param $widget_type
   *   The widget type.
   * @param array $elements
   *   An array of renderable elements.
   *
   * @return array|string
   *   The rendered or raw element properties.
   *
   * @throws \Exception
   */
  protected function renderElements($widget_type, array $elements) {
    switch ($widget_type) {
      case 'entity_property_formatter':
        $value = $elements;
        break;
      default:
        $value = (string) $this->renderer->renderPlain($elements);
    }

    return $value;
  }

  /**
   * Load entity view display.
   *
   * @param $entity_type_id
   *   An entity type identifier.
   * @param $bundle
   *   The entity bundle.
   * @param string $view_mode
   *   The entity display view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   *   The entity view display instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntityViewDisplay($entity_type_id, $bundle, $view_mode = 'default') {
    return $this
      ->entityTypeManager
      ->getStorage('entity_view_display')
      ->load("{$entity_type_id}.{$bundle}.{$view_mode}");
  }
}
