<?php

namespace Drupal\js_entity;

/**
 * Define JS entity process queue interface.
 */
interface JsEntityProcessQueueInterface {

  /**
   * Process all queue items.
   *
   * @return $this
   */
  public function processAll();

  /**
   * Process queue item.
   *
   * @param $item_id
   *   The queue item identifier.
   *
   * @return object
   *   The queue item object that's been processed.
   *
   * @throws \Exception
   */
  public function processItem($item_id);
}
