<?php

namespace Drupal\js_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Define JS entity render profile interface.
 */
interface JsEntityRenderProfileInterface {

  /**
   * Get render profile machine name.
   *
   * @return string
   *   The render profile machine name.
   */
  public function name();

  /**
   * Target entity type.
   *
   * @return string
   *   The target entity type identifier.
   */
  public function targetEntityType();

  /**
   * Target entity bundle.
   *
   * @return string
   *   The target entity bundle.
   */
  public function targetEntityBundle();

  /**
   * Get render profile caching configuration.
   *
   * @return array
   */
  public function getCaching();

  /**
   * Get render profile configuration.
   *
   * @return array
   *   An array of render configuration.
   */
  public function getConfig();

  /**
   * Get render profile view mode.
   *
   * @return string|null
   *   Get the render profile view mode.
   */
  public function getConfigViewMode();

  /**
   * Get render profile components.
   *
   * @return array
   *   Get the render profile components.
   */
  public function getConfigComponents();

  /**
   * Get render profile entity cache identifier.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   A content entity instance.
   *
   * @return string
   *   Return the render profile cache identifier.
   */
  public function getCacheId(ContentEntityInterface $entity);
}
