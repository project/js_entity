<?php

namespace Drupal\js_entity\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Define the JS entity render profile.
 *
 * @ConfigEntityType(
 *   id = "js_entity_render_profile",
 *   label = @Translation("Render Profile"),
 *   config_prefix = "render_profile",
 *   admin_permission = "administer js entity render profile",
 *   config_export = {
 *    "id",
 *    "label",
 *    "name",
 *    "caching",
 *    "configuration",
 *    "target_entity_type",
 *    "target_entity_bundle"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\js_entity\Form\JsEntityRenderProfileForm",
 *       "edit" = "\Drupal\js_entity\Form\JsEntityRenderProfileForm",
 *       "delete" = "\Drupal\js_entity\Form\JsEntityRenderProfileFormDelete"
 *     },
 *     "list_builder" = "\Drupal\js_entity\Controller\JsEntityRenderProfileList",
 *     "route_provider" = {
 *       "html" = "\Drupal\js_entity\Entity\Routing\JsEntityRouteDefault"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/js-entity/render-profile",
 *     "add-form" =  "/admin/config/system/js-entity/render-profile/add",
 *     "edit-form" = "/admin/config/system/js-entity/render-profile/{js_entity_render_profile}",
 *     "delete-form" = "/admin/config/system/js-entity/render-profile/{js_entity_render_profile}/delete"
 *   }
 * )
 */
class JsEntityRenderProfile extends ConfigEntityBase implements JsEntityRenderProfileInterface {

  /**
   * @var string
   */
  public $id;

  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $label;

  /**
   * @var string
   */
  public $target_entity_type;

  /**
   * @var string
   */
  public $target_entity_bundle;

  /**
   * @var array
   */
  public $caching = [];

  /**
   * @var array
   */
  public $configuration = [];

  /**
   * {@inheritdoc}
   */
  public function name() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function targetEntityType() {
    return $this->target_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function targetEntityBundle() {
    return $this->target_entity_bundle;
  }

  /**
   * {@inheritDoc}
   */
  public function getCaching() {
    return $this->caching;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigViewMode() {
    return isset($this->configuration['view_mode'])
      ? $this->configuration['view_mode']
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigComponents() {
    return isset($this->configuration['components'])
      ? $this->configuration['components']
      : [];
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    if (empty($this->target_entity_type)
      || empty($this->target_entity_bundle)
      || empty($this->name)) {
      return NULL;
    }

    return "{$this->targetEntityType()}.{$this->targetEntityBundle()}.{$this->name()}";
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheId(ContentEntityInterface $entity) {
    return "{$entity->getEntityTypeId()}:{$entity->bundle()}:{$this->id()}:{$entity->id()}";
  }

  /**
   * Determine if configuration exist already.
   *
   * @param $identifier
   *
   * @return array|int
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function entityExist($identifier) {
    return (bool) $this->getQuery()->condition('id', $identifier)->execute();
  }

  /**
   * Get cache tag excluded entities.
   *
   * @return array
   *   An array of cache tag excluded entities.
   */
  public function getCacheTagExcludedEntities() {
    $caching = $this->getCaching();

    if (!isset($caching['cache_tag'])) {
      return [];
    }
    $cache_tag = $caching['cache_tag'];

    if (!isset($cache_tag['excluded_entities'])) {
      return [];
    }

    return $cache_tag['excluded_entities'];
  }

  /**
   * Get entity storage query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getQuery() {
    return $this->getStorage()->getQuery();
  }

  /**
   * Get entity storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  protected function getStorage() {
    return $this->entityTypeManager()
      ->getStorage($this->getEntityTypeId());
  }
}
