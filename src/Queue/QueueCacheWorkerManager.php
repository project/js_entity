<?php

namespace Drupal\js_entity\Queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Queue\QueueWorkerManagerInterface;

/**
 * Define queue cache worker manager.
 */
class QueueCacheWorkerManager extends DefaultPluginManager implements QueueWorkerManagerInterface {

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/QueueCacheWorker',
      $namespaces,
      $module_handler,
      'Drupal\Core\Queue\QueueWorkerInterface',
      'Drupal\js_entity\Annotation\QueueCacheWorker'
    );

    $this->setCacheBackend($cache_backend, 'queue_cache_plugins');
    $this->alterInfo('queue_cache_info');
  }
}
