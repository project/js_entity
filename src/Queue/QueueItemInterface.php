<?php

namespace Drupal\js_entity\Queue;

/**
 * Define the queue item interface.
 */
interface QueueItemInterface {

  /**
   * Create the queue item.
   *
   * @return mixed
   */
  public function createItem();

  /**
   * Get queue item key.
   *
   * @return string
   *   The item key value.
   */
  public function getItemKey();

  /**
   * Get queue item cache tags.
   *
   * @return array
   */
  public function getItemCacheTags();

  /**
   * Set queue item key.
   *
   * @param $item_key
   *   A unique key for the create item.
   *
   * @return $this
   */
  public function setItemKey($item_key);
}
