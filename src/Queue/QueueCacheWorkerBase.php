<?php

namespace Drupal\js_entity\Queue;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define queue cache worker base.
 */
abstract class QueueCacheWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructor for the queue cache worker.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param $plugin_id
   *   The plugin identifier.
   * @param $plugin_definition
   *   The plugin definition instance.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CacheBackendInterface $cache_backend
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get(static::cacheBinServiceId($plugin_definition))
    );
  }

  /**
   * Cache bin service identifier.
   *
   * @param array $plugin_definition
   *   An array of the plugin definitions.
   *
   * @return string
   *   The cache bin service identifier.
   *
   * @throws \Exception
   */
  protected static function cacheBinServiceId(array $plugin_definition) {
    if (!isset($plugin_definition['cache_bin'])) {
      throw new \Exception(
        'Missing required cache bin directive.'
      );
    }
    $service_id = "cache.{$plugin_definition['cache_bin']}";

    if (!\Drupal::hasService($service_id)) {
      throw new \Exception(
        'The cache bin service identifier does not exist.'
      );
    }

    return $service_id;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\js_entity\JsEntityBuilderQueueItem $data */
    if (!is_object($data) || !is_subclass_of($data, QueueItemInterface::class)) {
      throw new \Exception(
        "Queue item wasn't formatted correctly. Please provide an item that 
        implements: \Drupal\js_entity\Queue\JsEntityBuildSettingsQueueItemInterface."
      );
    }
    $contents = $data->createItem();

    if (!isset($contents) || empty($contents)) {
      return;
    }

    $this->cacheBackend->set(
      $data->getItemKey(),
      $contents,
      $this->cacheExpire(),
      Cache::mergeTags(
        $data->getItemCacheTags(), $this->cacheTags()
      )
    );
  }

  /**
   * Get plugin definition cache bin.
   *
   * @return string|null
   */
  protected function cacheBin() {
    return isset($this->pluginDefinition['cache_bin'])
      ? $this->pluginDefinition['cache_bin']
      : NULL;
  }

  /**
   * Get plugin definition cache tags.
   *
   * @return array
   */
  protected function cacheTags() {
    return $this->pluginDefinition['cache_tags'];
  }

  /**
   * Get plugin definition cache expire.
   *
   * @return int
   */
  protected function cacheExpire() {
    return $this->pluginDefinition['cache_expire'];
  }

  /**
   * Get plugin definition cache bin prefix.
   *
   * @return string
   */
  protected function cacheBinPrefix() {
    return $this->pluginDefinition['cache_bin_prefix'];
  }
}
