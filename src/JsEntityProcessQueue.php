<?php

namespace Drupal\js_entity;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;

/**
 * Define the JS entity process queue.
 */
class JsEntityProcessQueue implements JsEntityProcessQueueInterface {

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  public function __construct(
    QueueInterface $queue,
    LoggerChannelInterface $logger,
    QueueWorkerManagerInterface $queue_worker_manager
  ) {
    $this->queue = $queue;
    $this->logger = $logger;
    $this->queueWorkerManager = $queue_worker_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function processAll() {
    while ($item = $this->queue->claimItem()) {
      $this->doProcess($item);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item_id) {
    $item = $this->queue->getItem($item_id);

    if ($item === FALSE || !isset($item)) {
      throw new \Exception(
        'Queue item is required for processing.'
      );
    }
    $this->doProcess($item);

    return $item;
  }

  /**
   * Run the queue process for the item.
   *
   * @param $item
   *   The queue item object.
   */
  protected function doProcess($item) {
    try {
      $this->getQueueWorkerInstance()->processItem($item->data);
      $this->queue->deleteItem($item);
    }
    catch (SuspendQueueException $exception) {
      $this->queue->releaseItem($item);
    }
    catch (\Exception $exception) {
      $this->logger->error($exception);
    }
  }

  /**
   * Get queue worker instance.
   *
   * @return \Drupal\Core\Queue\QueueWorkerInterface
   *   The JS entity cache worker instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getQueueWorkerInstance() {
    return $this->queueWorkerManager->createInstance('js_entity_cache');
  }
}
