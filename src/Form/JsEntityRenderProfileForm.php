<?php

namespace Drupal\js_entity\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define JS entity render profile form.
 */
class JsEntityRenderProfileForm extends EntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * JS entity render profile constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information.
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\js_entity\Entity\JsEntityRenderProfile $entity */
    $entity = $this->entity;

    $form['#prefix'] = '<div id="js-entity-render-profile">';
    $form['#suffix'] = '</div>';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Input a label for the render profile.'),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->name(),
      '#machine_name' => [
        'exists' => [$entity, 'entityExist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $target_entity_type = $this->getEntityFormValue(
      ['target_entity_type'], $form_state
    );
    $form['target_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#required' => TRUE,
      '#options' => $this->getEntityOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $target_entity_type,
      '#depth' => 1,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'js-entity-render-profile',
        'callback' => [$this, 'replaceEntityFormAjax']
      ]
    ];

    if (isset($target_entity_type) && !empty($target_entity_type)) {
      $target_entity_bundle = $this->getEntityFormValue(
        ['target_entity_bundle'], $form_state
      );
      $form['target_entity_bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle Type'),
        '#required' => TRUE,
        '#options' => $this->getEntityBundleOptions($target_entity_type),
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $target_entity_bundle,
        '#depth' => 1,
        '#ajax' => [
          'event' => 'change',
          'method' => 'replace',
          'wrapper' => 'js-entity-render-profile',
          'callback' => [$this, 'replaceEntityFormAjax']
        ]
      ];

      if (isset($target_entity_bundle) && !empty($target_entity_bundle)) {
        $form['configuration'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Render Configuration'),
          '#tree' => TRUE,
        ];

        $view_mode = $this->getEntityFormValue(
          ['configuration', 'view_mode'], $form_state, 'default'
        );
        $form['configuration']['view_mode'] = [
          '#type' => 'select',
          '#title' => $this->t('View Mode'),
          '#required' => TRUE,
          '#options' => $this->getEntityViewModeOptions($target_entity_type),
          '#empty_option' => $this->t('- Default -'),
          '#empty_value' => 'default',
          '#default_value' => $view_mode,
          '#depth' => 2,
          '#ajax' => [
            'event' => 'change',
            'method' => 'replace',
            'wrapper' => 'js-entity-render-profile',
            'callback' => [$this, 'replaceEntityFormAjax']
          ]
        ];

        if (isset($view_mode) && !empty($view_mode)) {
          $form['configuration']['components'] = [
            '#type' => 'table',
            '#header' => [
              'field_name' => $this->t('Field Name'),
              'property_name' => $this->t('Property Name'),
            ],
            '#empty' => $this->t(
              'The entity display "@view_mode" view mode has no components.',
              ['@view_mode' => $view_mode]
            ),
          ];

          $display = $this->getEntityViewDisplay(
            $target_entity_type, $target_entity_bundle, $view_mode
          );

          if ($display instanceof EntityDisplayInterface) {
            $components = $entity->getConfigComponents();

            foreach (array_keys($display->getComponents()) as $field_name) {
              $form['configuration']['components'][$field_name]['field_name'] = [
                '#plain_text' => $field_name,
              ];
              $form['configuration']['components'][$field_name]['property_name'] = [
                '#type' => 'textfield',
                '#required' => TRUE,
                '#default_value' => isset($components[$field_name]['property_name'])
                  ? $components[$field_name]['property_name']
                  : $field_name
              ];
            }
          }
        }
      }
    }
    $form['caching'] = [
      '#type' => 'details',
      '#title' => $this->t('Render Caching'),
      '#tree' => TRUE,
    ];
    $caching = $entity->getCaching();

    $form['caching']['cache_tag'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cache Tag'),
      '#tree' => TRUE,
    ];
    $form['caching']['cache_tag']['excluded_entities'] = [
      '#type' => 'select',
      '#title' => $this->t('Excluded Entities'),
      '#description' => $this->t('Select entity types that should be excluded 
        when aggregating cache tags.'),
      '#options' => $this->getEntityOptions(),
      '#multiple' => TRUE,
      '#default_value' => isset($caching['cache_tag']['excluded_entities'])
        ? $caching['cache_tag']['excluded_entities']
        : NULL
    ];

    return $form;
  }

  /**
   * Ajax replace entity form.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array form elements.
   */
  public function replaceEntityFormAjax(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();

    $element_depth = isset($element['#depth'])
      ? $element['#depth']
      : 1;

    return NestedArray::getValue(
      $form,
      array_splice($element['#array_parents'], 0, "-{$element_depth}")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $form_state->setRedirectUrl(
      $this->entity->toUrl('collection')
    );

    return $status;
  }

  /**
   * Get entity type bundle options.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity type bundle options.
   */
  protected function getEntityBundleOptions($entity_type_id) {
    $options = [];

    foreach ($this->getEntityBundles($entity_type_id) as $bundle => $info) {
      if (!isset($info['label'])) {
        continue;
      }
      $options[$bundle] = $info['label'];
    }

    return $options;
  }

  /**
   * Get entity bundles.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity bundle information.
   */
  protected function getEntityBundles($entity_type_id) {
    return $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
  }

  /**
   * Get entity view display.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   * @param $bundle
   *   The entity bundle.
   * @param string $view_mode
   *   The entity view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface|NULL
   *   The entity view display instance; otherwise NULL is returned.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityViewDisplay(
    $entity_type_id,
    $bundle,
    $view_mode = 'default'
  ) {
    return $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load("{$entity_type_id}.{$bundle}.{$view_mode}");
  }

  /**
   * Get entity form value.
   *
   * @param string $property
   *   The entity property name or an array of nested properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @param null $default
   *   The default entity form value.
   *
   * @return mixed|null
   *   The entity form value.
   */
  protected function getEntityFormValue($property, FormStateInterface $form_state, $default = NULL) {
    $property = !is_array($property)
      ? [$property]
      : $property;

    $key_exists = TRUE;
    $array = (array) $this->entity;

    $value = $form_state->hasValue($property)
      ? $form_state->getValue($property)
      : NestedArray::getValue($array, $property, $key_exists);

    if (isset($value) && $key_exists) {
      return $value;
    }

    return $default;
  }

  /**
   * Get entity options.
   *
   * @return array
   *   An array of entity options.
   */
  protected function getEntityOptions() {
    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      if (!$definition instanceof ContentEntityTypeInterface) {
        continue;
      }
      $options[$id] = $definition->getLabel();
    }

    return $options;
  }

  /**
   * Get entity view mode options.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of view mode options related to the entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityViewModeOptions($entity_type_id) {
    $options = [];

    /** @var \Drupal\Core\Entity\EntityViewModeInterface $view_mode */
    foreach ($this->getEntityViewModes($entity_type_id) as $view_mode) {
      if (!$view_mode->status()) {
        continue;
      }
      $identifier = $view_mode->id();
      $options[substr($identifier, strpos($identifier, '.') + 1)] = $view_mode->label();
    }

    return $options;
  }

  /**
   * Get entity view modes.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entity view mode instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityViewModes($entity_type_id) {
    return $this->entityTypeManager
      ->getStorage('entity_view_mode')
      ->loadByProperties([
        'targetEntityType' => $entity_type_id,
      ]);
  }
}
