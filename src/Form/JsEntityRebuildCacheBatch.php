<?php

namespace Drupal\js_entity\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

/**
 * Define JS entity rebuild cache batch.
 */
class JsEntityRebuildCacheBatch {

  /**
   * Rebuild JS entity cache.
   *
   * @param $entity_type
   *   The entity type.
   * @param $bundle
   *   The entity type bundle.
   * @param $profile_id
   *   The render profile identifier.
   * @param int $limit
   *   The limit of entities to process.
   * @param array $context
   *   An array of the batch context.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function rebuildCache($entity_type, $bundle, $profile_id, $limit, &$context) {
    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = \Drupal::service('cache.js_entity');
    /** @var \Drupal\Core\Logger\LoggerChannelInterface $logger */
    $logger = \Drupal::service('js_entity.logger');

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = \Drupal::entityTypeManager();

    $storage = $entity_type_manager
      ->getStorage($entity_type);

    $query = $storage->getQuery()->condition('type', $bundle);

    if (empty($context['sandbox'])) {
      $clone_query = clone $query;
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = $clone_query->count()->execute();
    }

    $entity_ids = $query
      ->range($context['sandbox']['progress'], $limit)
      ->execute();

    /** @var \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile */
    $render_profile = $entity_type_manager
      ->getStorage('js_entity_render_profile')
      ->load($profile_id);

    if (isset($render_profile)
      && $render_profile instanceof JsEntityRenderProfileInterface) {

      $entity_builder = static::jsEntityBuilder();

      foreach ($entity_ids as $entity_id) {
        $entity = $storage->load($entity_id);

        $context['sandbox']['current_id'] = $entity_id;
        $context['message'] = new TranslatableMarkup(
          'Rebuilding cache for "@label"', ['@label' => $entity->label()]
        );
        $cache_data = [];
        $cache_id = $render_profile->getCacheId($entity);

        try {
          $cache_data = $entity_builder
            ->build(
              $entity, $render_profile
            );
        }
        catch (\Exception $exception) {
          watchdog_exception('js_entity', $exception);
        }

        if (!empty($cache_data)) {
          $cache->set($cache_id, $cache_data, Cache::PERMANENT, []);
          $context['results'][] = $cache_id;
        }

        $context['sandbox']['progress']++;
      }

      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
    else {
      $logger->error(
        new TranslatableMarkup('Invalid JS entity render profile provided.')
      );
    }
  }

  /**
   * Rebuild cache finished callback.
   *
   * @param $success
   *   A boolean define if the batch was successful.
   * @param $results
   *   An array of batch results.
   * @param $operations
   *   An array of batch operations.
   */
  public static function rebuildCacheFinished($success, array $results, array $operations) {
    /** @var \Drupal\Core\Messenger\MessengerInterface $messenger */
    $messenger = \Drupal::service('messenger');

    if (!empty($results)) {
      $message = $success
        ? new TranslatableMarkup('JS entity caches were rebuilt successfully.')
        : new TranslatableMarkup("JS entity caches weren't rebuilt successfully.");

      $status = $success
        ? MessengerInterface::TYPE_STATUS
        : MessengerInterface::TYPE_ERROR;
    }
    else {
      $message = new TranslatableMarkup(
        'There were no JS entity caches rebuilt.'
      );
      $status = MessengerInterface::TYPE_WARNING;
    }

    $messenger->addMessage($message, $status);
  }

  /**
   * Get the JS entity builder.
   *
   * @return \Drupal\js_entity\JsEntityBuilderInterface
   */
  protected static function jsEntityBuilder() {
    return \Drupal::service('js_entity.builder');
  }
}
