<?php

namespace Drupal\js_entity\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define JS entity rebuild cache form.
 */
class JsEntityRebuildCacheForm extends FormBase {

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Js constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'js_entity_rebuild_cache';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="js-entity-rebuild-cache">';
    $form['#suffix'] = '</div>';

    $entity_type = $form_state->getValue('entity_type', NULL);

    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
      '#tree' => FALSE,
    ];
    $form['filters']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $this->getEntityTypeOptions(),
      '#required' => TRUE,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'js-entity-rebuild-cache',
        'callback' => [$this, 'jsEntityRebuildCacheAjax'],
      ],
      '#default_value' => $entity_type,
    ];

    if (!empty($entity_type)) {
      $form['filters']['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#required' => TRUE,
        '#options' => $this->getEntityBundleOptions($entity_type),
        '#ajax' => [
          'event' => 'change',
          'method' => 'replace',
          'wrapper' => 'js-entity-rebuild-cache',
          'callback' => [$this, 'jsEntityRebuildCacheAjax'],
        ],
      ];
      $bundle = $form_state->getValue('bundle');

      if (isset($bundle) && !empty($bundle)) {
        $form['filters']['render_profile'] = [
          '#type' => 'select',
          '#title' => $this->t('Render Profile'),
          '#required' => TRUE,
          '#options' => $this->getRenderProfileOptions($entity_type, $bundle)
        ];
      }
    }
    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('Select how many items to process in a batch.'),
      '#min' => 1,
      '#max' => 100,
      '#required' => TRUE,
      '#default_value' => 1,
    ];

    $form['actions']['#type'] = 'action';
    $form['actions']['build'] = [
      '#type' => 'submit',
      '#value' => $this->t('Rebuild Cache'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $limit = $form_state->getValue('limit', 1);
    $bundle = $form_state->getValue('bundle', NULL);
    $entity_type = $form_state->getValue('entity_type', NULL);
    $render_profile = $form_state->getValue('render_profile', NULL);

    if (!isset($entity_type) || !isset($bundle) || !isset($render_profile)) {
      return;
    }

    $batch = [
      'title' => $this->t('Rebuilding Entity Cache'),
      'operations' => [
        [
          'Drupal\js_entity\Form\JsEntityRebuildCacheBatch::rebuildCache',
          [$entity_type, $bundle, $render_profile, $limit]
        ]
      ],
      'finished' => 'Drupal\js_entity\Form\JsEntityRebuildCacheBatch::rebuildCacheFinished'
    ];

    batch_set($batch);
  }

  /**
   * Rebuild cache ajax callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   The form elements.
   */
  public function jsEntityRebuildCacheAjax(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Get entity type options.
   *
   * @return array
   *   An array of entity type options.
   */
  protected function getEntityTypeOptions() {
    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_id => $entity) {
      if (!$entity instanceof ContentEntityTypeInterface) {
        continue;
      }
      $options[$entity_id] = $entity->getLabel();
    }

    return $options;
  }

  /**
   * Get entity bundle options.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity bundle options.
   */
  protected function getEntityBundleOptions($entity_type_id) {
    $options = [];

    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$bundle] = $definition['label'];
    }

    return $options;
  }

  /**
   * Get entity render profile options.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   * @param $entity_bundle
   *   The entity type bundle.
   *
   * @return array
   *   An array of entity render profile options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRenderProfileOptions($entity_type_id, $entity_bundle) {
    $options = [];

    foreach ($this->getRenderProfiles($entity_type_id, $entity_bundle) as $id => $profile) {
      if (!$profile instanceof JsEntityRenderProfileInterface) {
        continue;
      }
      $options[$id] = $profile->label();
    }

    return $options;
  }

  /**
   * Get render profiles for a given entity type and bundle.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   * @param $entity_bundle
   *   The entity type bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of JS entity render profiles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRenderProfiles($entity_type_id, $entity_bundle) {
    return $this->entityTypeManager->getStorage('js_entity_render_profile')
      ->loadByProperties([
        'target_entity_type' => $entity_type_id,
        'target_entity_bundle' => $entity_bundle,
      ]);
  }
}
