<?php

namespace Drupal\js_entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

/**
 * Define JS entity cache lookup.
 */
class JsEntityCacheLookup implements JsEntityCacheLookupInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * @var \Drupal\js_entity\JsEntityProcessQueueInterface
   */
  protected $queueProcess;

  /**
   * JS entity cache lookup constructor.
   *
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   The queue instance.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger instance.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend instance.
   * @param \Drupal\js_entity\JsEntityProcessQueueInterface $queue_process
   *   The queue process instance.
   */
  public function __construct(
    QueueInterface $queue,
    LoggerChannelInterface $logger,
    CacheBackendInterface $cache_backend,
    JsEntityProcessQueueInterface $queue_process
  ) {
    $this->queue = $queue;
    $this->logger = $logger;
    $this->cacheBackend = $cache_backend;
    $this->queueProcess = $queue_process;
  }

  /**
   * {@inheritdoc}
   */
  public function entityLookup(
    ContentEntityInterface $entity,
    JsEntityRenderProfileInterface $render_profile
  ) {
    $cache_id = $render_profile->getCacheId($entity);

    $this->logger->info(
      $this->t('Running entity lookup for cache id: @cache_id',
        ['@cache_id' => $cache_id]
      )
    );
    $cache_object = $this->getCacheData($cache_id);

    if ($cache_object !== FALSE && isset($cache_object->data)) {
      return $cache_object->data;
    }

    $queue_item_id = $this->createEntityCacheQueue(
      $cache_id, $entity, $render_profile
    );

    return $this->processQueueItem($queue_item_id);
  }

  /**
   * Get cache object.
   *
   * @param $cache_id
   *   The cache identifier.
   *
   * @return boolean|object
   *   The cache object.
   */
  public function getCacheData($cache_id) {
    return $this->cacheBackend->get($cache_id);
  }

  /**
   * Create entity cache queue.
   *
   * @param $cache_id
   *   The cache identifier.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   * @param \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile
   *   The content entity render profile.
   *
   * @return string
   *   The queue item identifier.
   */
  protected function createEntityCacheQueue(
    $cache_id,
    ContentEntityInterface $entity,
    JsEntityRenderProfileInterface $render_profile
  ) {
    return $this->queue->createItem(
      (new JsEntityBuilderQueueItem(
        $entity,
        $render_profile
      ))->setItemKey($cache_id)
    );
  }

  /**
   * Process queue item.
   *
   * @param $item_id
   *   The queue item identifier.
   *
   * @return array
   *   The processed queue item data.
   *
   * @throws \Exception
   */
  protected function processQueueItem($item_id) {
    try {
      $item = $this->queueProcess->processItem($item_id);

      if (!isset($item) || $item === FALSE) {
        throw new \Exception(
          'Unable to execute queue process due to missing item.'
        );
      }
      /** @var \Drupal\js_entity\Queue\QueueItemInterface $item_data */
      $item_data = $item->data;
      $cache_object = $this->getCacheData($item_data->getItemKey());

      return $cache_object !== FALSE
        && isset($cache_object->data)
          ? $cache_object->data
          : [];
    } catch(\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }

    return [];
  }
}
