<?php

namespace Drupal\js_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

/**
 * Define JS entity lazy load.
 */
class JsEntityLazyLoad implements TrustedCallbackInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\js_entity\JsEntityCacheLookupInterface
   */
  protected $jsEntityCacheLookup;

  /**
   * JS entity lazy load constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\js_entity\JsEntityCacheLookupInterface $js_entity_cache_lookup
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    JsEntityCacheLookupInterface $js_entity_cache_lookup
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->jsEntityCacheLookup = $js_entity_cache_lookup;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return [
      'buildJsEntitySettingsLazy',
    ];
  }

  /**
   * Attach lazy builder element.
   *
   * @param $unique_key
   *   The unique key to compartmentalize settings.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity instance to retrieve data for.
   * @param \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile
   *   The entity render profile identifier.
   *
   * @return array
   *   A lazy_builder formatted array.
   */
  public function attachLazyBuilder(
    $unique_key,
    ContentEntityInterface $entity,
    JsEntityRenderProfileInterface $render_profile
  ) {
    $unique_key = $this->formatMachineName($unique_key);

    $element = [
      '#lazy_builder' => [
        'js_entity.lazy_load:buildJsEntitySettingsLazy', [
          $unique_key,
          $entity->id(),
          $entity->getEntityTypeId(),
          $render_profile->id()
        ]
      ],
    ];

    return $element;
  }

  /**
   * Build JS entity Drupal settings attached element.
   *
   * @param $unique_key
   *   The unique key to compartmentalize settings.
   * @param $entity_id
   *   The entity identifier.
   * @param $entity_type
   *   The entity type identifier.
   * @param $render_profile_id
   *   The entity render profile identifier.
   *
   * @return array
   *   An array element containing an attached drupalSettings for jsEntity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildJsEntitySettingsLazy(
    $unique_key,
    $entity_id,
    $entity_type,
    $render_profile_id
  ) {
    $entity = $this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id);

    if (!$entity instanceof ContentEntityInterface) {
      return [];
    }
    $render_profile = $this->loadRenderProfile($render_profile_id);

    $build['#attached'] = [
      'library' => [
        'core/drupal',
        'core/drupalSettings'
      ],
      'drupalSettings' => [
        'jsEntity' => [
          $unique_key => $this->jsEntityCacheLookup->entityLookup($entity, $render_profile)
        ]
      ]
    ];

    return $build;
  }

  /**
   * Load render profile.
   *
   * @param $profile_id
   *   The render profile identifier.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *    The entity render profile.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadRenderProfile($profile_id) {
    return $this->entityTypeManager
      ->getStorage('js_entity_render_profile')
      ->load($profile_id);
  }

  /**
   * Format string to a machine name.
   *
   * @param $string
   *   A string value.
   *
   * @return string|string[]|null
   */
  protected function formatMachineName($string) {
    return preg_replace(
      '@[^a-z0-9_]+@','_', strtolower($string)
    );
  }
}
