<?php

namespace Drupal\js_entity\Plugin\QueueCacheWorker;

use Drupal\js_entity\Annotation\QueueCacheWorker;
use Drupal\js_entity\Queue\QueueCacheWorkerBase;

/**
 * Define cache queue worker.
 *
 * @QueueCacheWorker(
 *   id = "js_entity_cache",
 *   cache_bin = "js_entity"
 * )
 */
class JsEntityCacheQueueWorker extends QueueCacheWorkerBase {}
