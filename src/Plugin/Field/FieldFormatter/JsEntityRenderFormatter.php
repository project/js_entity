<?php

namespace Drupal\js_entity\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;
use Drupal\js_entity\JsEntityLazyLoad;
use Drupal\token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * JS entity render field  formatter.
 *
 * @FieldFormatter(
 *   id = "js_entity_rendered",
 *   label = @Translation("Rendered JS Entity"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class JsEntityRenderFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\token\Token
   */
  protected $token;

  /**
   * @var \Drupal\js_entity\JsEntityLazyLoad
   */
  protected $jsEntityLazyLoad;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    TokenInterface $token,
    JsEntityLazyLoad $js_entity_lazy_load,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->token = $token;
    $this->jsEntityLazyLoad  = $js_entity_lazy_load;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static (
      $plugin_id, 
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('token'),
      $container->get('js_entity.lazy_load'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'unique_key' => NULL,
      'render_profile' => NULL
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['render_profile'] = [
      '#type' => 'select',
      '#title' => $this->t('Render Profile'),
      '#description' => $this->t('Select a entity render profile.'),
      '#required' => TRUE,
      '#options' => $this->getEntityReferenceRenderProfileOptions(),
      '#default_value' => $this->getSetting('render_profile')
    ];
    $form['unique_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unique Key'),
      '#description' => $this->t('Input a unique key namespace to 
        compartmentalize the data. Only use lowercase and underscores. <br/> 
        <strong>NOTE:</strong> This field supports token replacements.'),
      '#required' => TRUE,
      '#size' => 25,
      '#default_value' => $this->getSetting('unique_key')
    ];
    $form['token_replacement'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        $this->getFieldTargetTypeId(),
        $this->getReferenceTargetTypeId(),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this->t('<strong>Unique Key:</strong> @unique_key', [
      '@unique_key' => $this->getSetting('unique_key')
    ]);
    $render_profile = $this->getSetting('render_profile');
    $render_options = $this->getEntityReferenceRenderProfileOptions();

    $summary[] = $this->t('<strong>Render Profile:</strong> @render_profile', [
      '@render_profile' => $render_options[$render_profile]
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $render_profile = $this->getEntityRenderProfile();
    $target_settings = $this->getReferenceTargetSettings();

    foreach ($items as $delta => $item) {
      if (!$item instanceof EntityReferenceItem
        || !$item->entity instanceof EntityInterface) {
        continue;
      }
      $entity = $item->entity;

      if (!in_array($entity->bundle(), $target_settings['target_bundles'])) {
        continue;
      }
      $unique_key = $this->getUniqueKey($item);

      $elements[$delta] = $this
        ->jsEntityLazyLoad
        ->attachLazyBuilder($unique_key, $entity, $render_profile);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_settings = $field_definition->getSetting('handler_settings');

    if (!isset($target_settings['target_bundles'])) {
      return FALSE;
    }
    $target_type = $field_definition->getSetting('target_type');
    $target_bundles = $target_settings['target_bundles'];

    $profile_storage = \Drupal::entityTypeManager()
      ->getStorage('js_entity_render_profile');

    $profile_count = $profile_storage->getQuery()
      ->condition('target_entity_type', $target_type)
      ->condition('target_entity_bundle', $target_bundles, 'IN')
      ->count()
      ->execute();

    return $profile_count >= 1;
  }

  /**
   * Get entity render profile instance.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity render profile.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityRenderProfile() {
    return $this->loadEntityRenderProfile(
      $this->getSetting('render_profile')
    );
  }

  /**
   * Get unique key.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item instance.
   *
   * @return string|null
   *   The JS entity unique key.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getUniqueKey(FieldItemInterface $item) {
    $data = [];

    if ($entity = $item->getEntity()) {
      $data[$this->getFieldTargetTypeId()] = $entity;
    }

    if ($entity = $item->get('entity')) {
      $data[$this->getReferenceTargetTypeId()] = $entity;
    }
    $unique_key = $this->getSetting('unique_key');

    return isset($unique_key)
      ? $this->token->replace($unique_key, $data, ['clear' => TRUE])
      : NULL;
  }

  /**
   * Get entity reference render profile options.
   *
   * @return array
   *   An array of render profiles related to the entity reference.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityReferenceRenderProfileOptions() {
    $options = [];
    $settings = $this->getReferenceTargetSettings();

    foreach ($settings['target_bundles'] as $bundle) {
      $options = array_merge($options, $this->getEntityRenderProfileOptions(
        $this->getReferenceTargetTypeId(),
        $bundle
      ));
    }

    return $options;
  }

  /**
   * Get entity render profile options.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   * @param $entity_bundle
   *   The entity type bundle.
   *
   * @return array
   *   An array of entity render profile options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityRenderProfileOptions($entity_type_id, $entity_bundle) {
    $options = [];

    foreach ($this->getRenderProfiles($entity_type_id, $entity_bundle) as $id => $profile) {
      if (!$profile instanceof JsEntityRenderProfileInterface) {
        continue;
      }
      $options[$id] = $profile->label();
    }

    return $options;
  }

  /**
   * Get render profiles for a given entity type and bundle.
   *
   * @param $entity_type_id
   *   The entity type identifier.
   * @param $entity_bundle
   *   The entity type bundle.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of JS entity render profiles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRenderProfiles($entity_type_id, $entity_bundle) {
    return $this->entityTypeManager->getStorage('js_entity_render_profile')
      ->loadByProperties([
        'target_entity_type' => $entity_type_id,
        'target_entity_bundle' => $entity_bundle,
      ]);
  }

  /**
   * Load entity render profile.
   *
   * @param $profile_id
   *   The entity render profile identifier.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   An entity render profile.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntityRenderProfile($profile_id) {
    return $this->entityTypeManager
      ->getStorage('js_entity_render_profile')
      ->load($profile_id);
  }

  /**
   * Get reference target entity type identifier.
   *
   * @return string|null
   *   The entity reference target entity identifier.
   */
  protected function getReferenceTargetTypeId() {
    return $this->getFieldSetting('target_type') ?: NULL;
  }

  /**
   * @return array
   */
  protected function getReferenceTargetSettings() {
    return $this->getFieldSetting('handler_settings') ?: [];
  }

  /**
   * Get the field target entity type identifier.
   *
   * @return string
   *   The field target entity type identifier.
   */
  protected function getFieldTargetTypeId() {
    return $this->fieldDefinition->getTargetEntityTypeId();
  }
}
