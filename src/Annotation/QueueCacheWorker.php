<?php

namespace Drupal\js_entity\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Cache\Cache;

/**
 * Define the queue cache worker annotation.
 *
 * @Annotation
 */
class QueueCacheWorker extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The cache bin.
   *
   * @var string
   */
  public $cache_bin;

  /**
   * The cache tags.
   *
   * @var array
   */
  public $cache_tags = [];

  /**
   * The cache expire.
   *
   * @var int
   */
  public $cache_expire = Cache::PERMANENT;

  /**
   * The cache bin prefix.
   *
   * @var string
   */
  public $cache_bin_prefix = 'cache';
}
