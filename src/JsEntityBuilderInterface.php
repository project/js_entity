<?php

namespace Drupal\js_entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\js_entity\Entity\JsEntityRenderProfileInterface;

/**
 * Define JS entity builder.
 */
interface JsEntityBuilderInterface {

  /**
   * Build entity JS settings.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   A content entity instance.
   * @param \Drupal\js_entity\Entity\JsEntityRenderProfileInterface $render_profile
   *   A content entity render profile instance.
   *
   * @return array
   *   An array of the entity properties, keyed by the unique name.
   *
   */
  public function build(ContentEntityInterface $entity, JsEntityRenderProfileInterface $render_profile);
}
