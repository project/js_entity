<?php

namespace Drupal\js_entity;

use Drupal\Core\Queue\DatabaseQueue;

/**
 * Define JS entity database queue.
 */
class JsEntityDatabaseQueue extends DatabaseQueue {

  const TABLE_NAME = 'js_entity_queue';

  /**
   * {@inheritdoc}
   */
  public function schemaDefinition() {
    $schema = parent::schemaDefinition();
    $schema['fields']['checksum'] = [
      'type' => 'varchar',
      'description' => 'Queue data md5 checksum.',
      'length' => '32',
      'not null' => TRUE,
    ];

    return $schema;
  }

  /**
   * Get queue item object.
   *
   * @param $item_id
   *   The item identifier.
   *
   * @return object|boolean
   *   The queue item object; otherwise FALSE if not found.
   */
  public function getItem($item_id) {
    $item = $this->connection->query(
      'SELECT data, created, item_id FROM {' . static::TABLE_NAME . '} WHERE item_id = :item_id',
      [':item_id' => $item_id]
    )->fetchObject();

    return $this->formatItem($item);
  }

  /**
   * Get queue item by checksum.
   *
   * @param $checksum
   *   The queue item checksum.
   *
   * @return object|boolean
   *   The queue item object; otherwise FALSE if not found.
   */
  protected function getItemByChecksum($checksum) {
    $item = $this->connection->query(
      'SELECT data, created, item_id FROM {' . static::TABLE_NAME . '} WHERE checksum = :checksum',
      [':checksum' => $checksum]
    )->fetchObject();

    return $this->formatItem($item);
  }

  /**
   * {@inheritdoc}
   */
  protected function doCreateItem($data) {
    $input = serialize($data);
    $checksum = md5($input);

    if (!$this->hasChecksum($checksum)) {
      $query = $this->connection->insert(static::TABLE_NAME)
        ->fields([
          'name' => $this->name,
          'data' => $input,
          // We cannot rely on REQUEST_TIME because many items might be created
          // by a single request which takes longer than 1 second.
          'created' => time(),
          'checksum' => $checksum
        ]);

      return $query->execute();
    }

    return $this->getItemByChecksum($checksum)->item_id;
  }

  /**
   * Format item object.
   *
   * @param object $item
   *   The queue item object.
   *
   * @return object|boolean
   *   The queue item object; otherwise FALSE if not found.
   */
  protected function formatItem($item) {
    if ($item !== FALSE && isset($item->data)) {
      $item->data = unserialize($item->data);
    }

    return $item;
  }

  /**
   * Queue has checksum already existing.
   *
   * @param $checksum
   *   The MD5 hash checksum.
   *
   * @return boolean
   *   Return TRUE if checksum exists already; otherwise FALSE.
   */
  protected function hasChecksum($checksum) {
    return (boolean) $this->connection->query(
      'SELECT 1 FROM {' . static::TABLE_NAME . '} WHERE checksum = :checksum',
      [':checksum' => $checksum]
    )->fetchField();
  } 
}
