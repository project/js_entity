<?php

/**
 * @file
 * The drush 8 command implementations.
 */

/**
 * Implements hook_drush_command().
 */
function js_entity_drush_command() {
  $items = [];
  $items['js-entity-rebuild'] = [
    'description' => 'Rebuild the JS entity cache.',
    'aliases' => ['jer'],
    'arguments' => [
      'entity-type' => 'Input an entity type',
      'entity-bundle' => 'Input an entity bundle',
      'render-profile' => 'Input an entity render profile'
    ],
    'options' => [
      'limit' => [
        'description' => 'Input how many items to process during the rebuild.',
        'value' => TRUE,
      ]
    ],
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL
  ];

  return $items;
}

/**
 * Callback for the JS entity rebuild command.
 *
 * @param $entity_type
 *   The entity type.
 * @param $entity_bundle
 *   The entity bundle.
 * @param $render_profile
 *   The JS entity render profile.
 */
function drush_js_entity_rebuild($entity_type, $entity_bundle, $render_profile) {
  $limit = drush_get_option('limit', 1);
  $profile_id = "{$entity_type}.{$entity_bundle}.{$render_profile}";

  $batch = [
    'title' => dt('Rebuilding Entity Cache'),
    'operations' => [
      [
        'Drupal\js_entity\Form\JsEntityRebuildCacheBatch::rebuildCache',
        [$entity_type, $entity_bundle, $profile_id, $limit]
      ]
    ],
    'finished' => 'Drupal\js_entity\Form\JsEntityRebuildCacheBatch::rebuildCacheFinished'
  ];

  batch_set($batch);

  drush_backend_batch_process();
}
